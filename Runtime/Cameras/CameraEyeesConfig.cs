﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEyeesConfig : MonoBehaviour
{
    public Camera m_left;
    public Camera m_right;

    public Color m_backgroundColor = Color.white;
    public float m_fieldOfView=60;
    public float m_nearDistance=0.01f;
    public float m_farDistance=1000;

    public void SetCameras(Camera left, Camera right) {
        m_left = left;
        m_right = right;
    }

    public void SetFieldOfView(float angle)
    {
        m_fieldOfView = angle;
        m_left.fieldOfView = m_right.fieldOfView = angle;
    }
    public void SetColor(Color color)
    {
        m_backgroundColor = color;
        m_left.backgroundColor = m_right.backgroundColor = color;
    }
    public void SetCameraViewDistance(float nearDistance, float farDistance)
    {
        m_farDistance = farDistance;
        m_nearDistance = nearDistance;
        m_left.nearClipPlane = m_right.nearClipPlane = nearDistance;
        m_left.farClipPlane = m_right.farClipPlane = farDistance;
    }
    public void SetHDR(bool hdr)
    {
        m_left.allowHDR = m_right.allowHDR = hdr;
    }
    public void SetMSAA(bool msaa)
    {
        m_left.allowMSAA = m_right.allowMSAA = msaa;
    }

    private void OnValidate()
    {
        SetColor(m_backgroundColor);
        SetFieldOfView(m_fieldOfView);
        SetCameraViewDistance(m_nearDistance, m_farDistance);
    }
}
