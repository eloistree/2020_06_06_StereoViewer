﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StereoCameraTurnAroundManager : MonoBehaviour
{
    public CameraObjectifDistance m_positionOfObjectif;
    public CameraEyeesDistance m_eyesDistance;
    public CameraEyeesConfig m_configuration;

    public void SetDistanceOfObjectif(float distance) { m_positionOfObjectif.SetDistance(distance); }
    public void SetDistanceBetweenEyes(float distance) { m_eyesDistance.SetEyesDistance(distance); }
    public void SetBackgroundColor(Color color) { m_configuration.SetColor(color); }
    public void SetCameraRotation(float horizontal, float vertical) { m_positionOfObjectif.SetLocalRotation(horizontal, vertical); }

    public void SetVertical(float angle)
    {
        m_positionOfObjectif.SetVertical(angle);
    }

    public void SetFieldOfView(float angle)
    {
        m_configuration.SetFieldOfView(angle);
    }

    public void SetHorizontal(float angle)
    {
        m_positionOfObjectif.SetHorizontal(angle);
    }
}
