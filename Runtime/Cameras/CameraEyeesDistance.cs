﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEyeesDistance : MonoBehaviour {

    [Range(0f,42f)]
    public float m_eyeesDistance = 0.06f;
    public Transform m_leftCameraAnchor;
    public Transform m_rightCameraAnchor;

    public void SetEyesDistance(float distance) {
        m_eyeesDistance = distance;
        RefreshCameraPosition();
    }

    public void OnValidate()
    {
        RefreshCameraPosition();
    }
    public void    RefreshCameraPosition()
    {
        m_leftCameraAnchor.localPosition = new Vector3(-m_eyeesDistance / 2f, 0, 0);
        m_rightCameraAnchor.localPosition = new Vector3(m_eyeesDistance / 2f, 0, 0);
        m_leftCameraAnchor.localRotation = Quaternion.identity;
        m_rightCameraAnchor.localRotation = Quaternion.identity;
    }
}
