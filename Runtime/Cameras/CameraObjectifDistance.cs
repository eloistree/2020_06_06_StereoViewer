﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraObjectifDistance : MonoBehaviour
{
    public Transform m_toMove;
    public float m_horizontalAngle;
    public float m_verticalAngle;
    public float m_distance;
    public bool m_inverseHorizontal;
    public bool m_inverseVertical;

    public void SetDistance(float distance)
    {
        m_distance = Mathf.Abs(distance);
        SetLocalRotation(m_horizontalAngle, m_verticalAngle, m_distance);
    }
    public void SetLocalRotation(float euleurHorizontalAngle, float eulerVerticalAngle, float distanceOfObjectif)
    {
        m_horizontalAngle = euleurHorizontalAngle;
        m_verticalAngle = eulerVerticalAngle;
        m_distance = distanceOfObjectif;
        Quaternion localRotation = Quaternion.Euler(eulerVerticalAngle * (m_inverseVertical ? -1f : 1f), euleurHorizontalAngle *(m_inverseHorizontal?-1f:1f),  0);
        m_toMove.localPosition = localRotation * (Vector3.forward*m_distance);
        m_toMove.localRotation = localRotation * Quaternion.Euler(0f, 180f, 0f);
    }

    public void SetLocalRotation(float horizontal, float vertical)
    {
        SetLocalRotation(horizontal, vertical, m_distance);
    }
    public void SetHorizontal(float horizontal)
    {
        SetLocalRotation(horizontal, m_verticalAngle, m_distance);
    }
    public void SetVertical(float vertical)
    {
        SetLocalRotation(m_horizontalAngle, vertical, m_distance);
    }

    private void OnValidate()
    {
        SetLocalRotation(m_horizontalAngle, m_verticalAngle, m_distance);
    }
}
