﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_InterfaceLRCamera : MonoBehaviour
{
    public StereoCameraTurnAroundManager m_stereoCameraManager;


    public bool CheckInSceneForCameraIfNull() {

        if (m_stereoCameraManager == null)
            m_stereoCameraManager = GameObject.FindObjectOfType<StereoCameraTurnAroundManager>();
        return m_stereoCameraManager != null;
    }
    public void SetDistance(float distance)
    {
        if (!CheckInSceneForCameraIfNull()) return;
        m_stereoCameraManager.SetDistanceOfObjectif(distance);

    }
    public void SetFieldOfView(float angle)
    {
        if (!CheckInSceneForCameraIfNull()) return;

        m_stereoCameraManager.SetFieldOfView(angle);

    }
    public void SetHorizontal(float angle)
    {
        if (!CheckInSceneForCameraIfNull()) return;

        m_stereoCameraManager.SetHorizontal(angle);
    }
    public void SetVertical(float angle)
    {
        if (!CheckInSceneForCameraIfNull()) return;

        m_stereoCameraManager.SetVertical(angle);
    }
    public void SetEyesDistance(float distance)
    {
        if (!CheckInSceneForCameraIfNull()) return;
        m_stereoCameraManager.SetDistanceBetweenEyes(distance);

    }
}
