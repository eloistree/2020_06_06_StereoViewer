﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ListenToSliderRelease : MonoBehaviour  {

    public Slider m_slider;
    public bool m_hasBeenDragUp;
    public SliderEvent m_onStartDragging;
    public SliderEvent m_onRelease;
    public float m_cooldown=0.8f;
    public  float m_cooldownState;
    void Start ()
    {
        m_slider.onValueChanged.AddListener(ChangeVideoFrame);
    }
    public void Update()
    {
        if (m_cooldownState > 0f) {
            m_cooldownState -= Time.deltaTime;
            if (m_cooldownState < 0f) {
                m_cooldownState = 0;
                 m_hasBeenDragUp = true;
            }
        }
        if (!m_hasBeenDragUp) return;
        m_hasBeenDragUp = false;
        m_onRelease.Invoke(m_slider.value);
    }

    private void ChangeVideoFrame(float value)
    {
        if (m_cooldownState <= 0)
            m_onStartDragging.Invoke(value);
        m_cooldownState = m_cooldown;
    }
    [System.Serializable]
    public class SliderEvent : UnityEvent<float> { };

}
